// Functions Demo
// Jason Clemons

#include <iostream>
#include <conio.h>

// prototypes
float Add(float num1, float num2);
float Subtract(float num1, float num2);
float Multiply(float num1, float num2);
bool  Divide(float num1, float num2, float& answer);
void PerfOperation(void);


// main 
int main()
{
	// perform operation
	PerfOperation();
	
	// hold open for user input
	(void)_getch();
	return 0;
}

void PerfOperation(void)
{
	// variables
	char operation = ' ';
	double Onum1 = 0;
	double Onum2 = 0;
	bool runToken = 1;
	char doAgain = ' ';
	double result = 0;
	bool devided = 0;
	float answer = 0;

	if (runToken == 1)
	{
		// get user input
		std::cout << "What is the first number you'd like to use? (up to 2 decimals ex: 5.56)\n";
		std::cin >> Onum1;
		std::cout << "What type of math would you like to do? (You can use + - * / )\n";
		std::cin >> operation;
		std::cout << "What is the second number you'd like to operate on? (up to 2 decimals ex: 7.62)\n";
		std::cin >> Onum2;

		// perform operation conditionally based on input
		switch (operation)
		{

		case '+': // Add
			std::cout << "The answer is " << Add(Onum1, Onum2) << "\n";
			break;

		case '-': // subtract
			std::cout << "The answer is " << Subtract(Onum1, Onum2) << "\n";
			break;

		case '*': // multiply
			std::cout << "The answer is " << Multiply(Onum1, Onum2) << "\n";
			break;

		case '/': // divide if num2 not zero
			if (Divide(Onum1, Onum2, answer))
			{	
				// show answer
				std::cout << "The answer is " << answer << "\n";
				break;
			}
			else
			{
				// alert invalid
				std::cout << "A number can not be devided by Zero.\n";
				break;
			};
		default:
			// catch for other inputs
			std::cout << "You did not enter a valid operator\n";
		}


		// prompt to do another problem
		std::cout << "Would you like to perform another operation? ( y/n )\n";
		std::cin >> doAgain;

		// reapeat as needed
		if (doAgain == 'y'||doAgain == 'Y'){PerfOperation();};
	}
	else
	{
		return;
	};	
}

// add functino
float Add(float num1, float num2) 
{
	return num1 + num2;
}

// subtract function
float Subtract(float num1, float num2) 
{
	return num1 - num2;
}

// multiply function
float Multiply(float num1, float num2) 
{
	return num1 * num2;
}

// divide function
bool  Divide(float num1, float num2, float &answer)
{
	if (num2 != 0)
	{
		answer = num1 / num2;
		return true; 
	} 
	else {return false;}
}